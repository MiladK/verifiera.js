/* tslint:disable:arrow-call-signature */

import { assert } from 'chai'
import { describe, it } from 'mocha'

import { sweden } from './index'

describe('Sweden', () => {
  describe('getGender', () => {
    it('Gets the correct gender', (done) => {
      assert.strictEqual(sweden('571124-1322').getGender(), 'f')
      assert.strictEqual(sweden('890729-6746').getGender(), 'f')
      assert.strictEqual(sweden('680731-1003').getGender(), 'f')
      assert.strictEqual(sweden('111228+7568').getGender(), 'f')
      assert.strictEqual(sweden('160718-7562').getGender(), 'f')

      assert.strictEqual(sweden('470304-2657').getGender(), 'm')
      assert.strictEqual(sweden('650428-0196').getGender(), 'm')
      assert.strictEqual(sweden('671017-1239').getGender(), 'm')
      assert.strictEqual(sweden('670919-9530').getGender(), 'm')
      assert.strictEqual(sweden('811228-9874').getGender(), 'm')
      assert.strictEqual(sweden('880229-9878').getGender(), 'm')

      done()
    })

    it('Invalid personal numbers return empty gender', (done) => {
      assert.strictEqual(sweden('311280-888U').getGender(), '')

      done()
    })

    it('strict = false', (done) => {
      assert.strictEqual(sweden('19571124-1322', false).getGender(), 'f')
      assert.strictEqual(sweden('19890729-6746', false).getGender(), 'f')
      assert.strictEqual(sweden('19680731-1003', false).getGender(), 'f')
      assert.strictEqual(sweden('19111228+7568', false).getGender(), 'f')
      assert.strictEqual(sweden('20160718-7562', false).getGender(), 'f')

      assert.strictEqual(sweden('19470304-2657', false).getGender(), 'm')
      assert.strictEqual(sweden('19650428-0196', false).getGender(), 'm')
      assert.strictEqual(sweden('19671017-1239', false).getGender(), 'm')
      assert.strictEqual(sweden('19670919-9530', false).getGender(), 'm')
      assert.strictEqual(sweden('19811228-9874', false).getGender(), 'm')

      done()
    })
  })

  describe('getCensored', () => {
    it('Censors personal numbers', (done) => {
      assert.strictEqual(sweden('571124-1322').getCensored(), '571124-****')
      assert.strictEqual(sweden('470304-2657').getCensored(), '470304-****')
      assert.strictEqual(sweden('650428-0196').getCensored(), '650428-****')
      assert.strictEqual(sweden('890729-6746').getCensored(), '890729-****')
      assert.strictEqual(sweden('671017-1239').getCensored(), '671017-****')
      assert.strictEqual(sweden('680731-1003').getCensored(), '680731-****')
      assert.strictEqual(sweden('670919-9530').getCensored(), '670919-****')
      assert.strictEqual(sweden('811228-9874').getCensored(), '811228-****')
      assert.strictEqual(sweden('111228+7568').getCensored(), '111228+****')
      assert.strictEqual(sweden('160718-7562').getCensored(), '160718-****')
      assert.strictEqual(sweden('880229-9878').getCensored(), '880229-****')

      done()
    })

    it('Invalid personal numbers return empty string', (done) => {
      assert.strictEqual(sweden('311280-888U').getCensored(), '')

      done()
    })

    it('strict = false', (done) => {
      assert.strictEqual(sweden('19571124-1322', false).getCensored(), '19571124-****')
      assert.strictEqual(sweden('19470304-2657', false).getCensored(), '19470304-****')
      assert.strictEqual(sweden('19650428-0196', false).getCensored(), '19650428-****')
      assert.strictEqual(sweden('19890729-6746', false).getCensored(), '19890729-****')
      assert.strictEqual(sweden('19671017-1239', false).getCensored(), '19671017-****')
      assert.strictEqual(sweden('19680731-1003', false).getCensored(), '19680731-****')
      assert.strictEqual(sweden('19670919-9530', false).getCensored(), '19670919-****')
      assert.strictEqual(sweden('19811228-9874', false).getCensored(), '19811228-****')
      assert.strictEqual(sweden('19111228+7568', false).getCensored(), '19111228+****')
      assert.strictEqual(sweden('20160718-7562', false).getCensored(), '20160718-****')

      done()
    })
  })

  describe('getAge', () => {
    it('Calculates correct age', (done) => {
      const today = '2017-07-19'

      assert.strictEqual(sweden('761623-5572').getAge(today), 0)
      assert.strictEqual(sweden('160718-7562').getAge(today), 1)
      assert.strictEqual(sweden('111228-7568').getAge(today), 5)
      assert.strictEqual(sweden('811228-9874').getAge(today), 35)
      assert.strictEqual(sweden('650428-0196').getAge(today), 52)
      assert.strictEqual(sweden('470304-2657').getAge(today), 70)
      assert.strictEqual(sweden('160718+7562').getAge(today), 101)
      assert.strictEqual(sweden('111228+7568').getAge(today), 105)

      done()
    })

    it('Calculates age correctly with leap years', (done) => {
      assert.strictEqual(sweden('880229-9878').getAge('2020-02-28'), 31)
      assert.strictEqual(sweden('880229-9878').getAge('2020-02-29'), 32)

      done()
    })

    it('Invalid personal numbers return 0 age', (done) => {
      const today = '2017-07-19'

      assert.strictEqual(sweden('').getAge(today), 0)

      done()
    })

    it('strict = false', (done) => {
      const today = '2017-07-19'

      assert.strictEqual(sweden('19650428-0196', false).getAge(today), 52)
      assert.strictEqual(sweden('19470304-2657', false).getAge(today), 70)
      assert.strictEqual(sweden('19811228-9874', false).getAge(today), 35)
      assert.strictEqual(sweden('19111228+7568', false).getAge(today), 105)
      assert.strictEqual(sweden('20111228-7568', false).getAge(today), 5)
      assert.strictEqual(sweden('20160718-7562', false).getAge(today), 1)
      assert.strictEqual(sweden('19160718+7562', false).getAge(today), 101)

      done()
    })
  })

  describe('Validation', () => {
    it('Invalidates empty strings', (done) => {
      assert.isFalse(sweden('').validate())

      done()
    })

    it('Identifies invalid personal numbers', (done) => {
      assert.isFalse(sweden('571124-1324').validate())
      assert.isFalse(sweden('470304-2653').validate())
      assert.isFalse(sweden('890729-6743').validate())
      assert.isFalse(sweden('160718-7566').validate())

      done()
    })

    it('Identifies valid personal numbers', (done) => {
      assert.isTrue(sweden('571124-1322').validate())
      assert.isTrue(sweden('470304-2657').validate())
      assert.isTrue(sweden('650428-0196').validate())
      assert.isTrue(sweden('890729-6746').validate())
      assert.isTrue(sweden('671017-1239').validate())
      assert.isTrue(sweden('680731-1003').validate())
      assert.isTrue(sweden('670919-9530').validate())
      assert.isTrue(sweden('811228-9874').validate())
      assert.isTrue(sweden('111228+7568').validate())
      assert.isTrue(sweden('160718-7562').validate())
      assert.isTrue(sweden('880229-9878').validate())

      done()
    })

    describe('strict = false', () => {
      it('Invalidates empty strings', (done) => {
        assert.isFalse(sweden('', false).validate())

        done()
      })

      it('Identifies invalid personal numbers', (done) => {
        assert.isFalse(sweden('194703042656', false).validate())
        assert.isFalse(sweden('19470304-2656', false).validate())

        done()
      })

      it('Identifies valid personal numbers', (done) => {
        assert.isTrue(sweden('19571124-1322', false).validate())
        assert.isTrue(sweden('5711241322', false).validate())
        assert.isTrue(sweden('195711241322', false).validate())
        assert.isTrue(sweden('19470304-2657', false).validate())
        assert.isTrue(sweden('194703042657', false).validate())
        assert.isTrue(sweden('19650428-0196', false).validate())
        assert.isTrue(sweden('19890729-6746', false).validate())
        assert.isTrue(sweden('19671017-1239', false).validate())
        assert.isTrue(sweden('19680731-1003', false).validate())
        assert.isTrue(sweden('19670919-9530', false).validate())
        assert.isTrue(sweden('19811228-9874', false).validate())

        done()
      })
    })
  })

  describe('Samordningsnummer / The coordination number', () => {
    describe('Validation', () => {
      it('Identifies valid personal numbers', (done) => {
        assert.isTrue(sweden('701073-2399').validate())
        assert.isTrue(sweden('701063-2391').validate())

        done()
      })
    })

    describe('getGender', () => {
      it('Gets the correct gender', (done) => {
        assert.strictEqual(sweden('701073-2399').getGender(), 'm')
        assert.strictEqual(sweden('701063-2391').getGender(), 'm')

        done()
      })
    })

    describe('getCensored', () => {
      it('Gets the correct censored', (done) => {
        assert.strictEqual(sweden('701073-2399').getCensored(), '701073-****')
        assert.strictEqual(sweden('701063-2391').getCensored(), '701063-****')

        done()
      })
    })

    describe('getAge', () => {
      const today = '2017-07-19'

      it('Gets the correct age', (done) => {
        assert.strictEqual(sweden('701073-2399').getAge(today), 46)
        assert.strictEqual(sweden('701063-2391').getAge(today), 46)

        done()
      })
    })

    describe('getBirthday', () => {
      it('Gets the correct birthday', (done) => {
        assert.strictEqual(sweden('701073-2399').getBirthday(), '1970-10-13')
        assert.strictEqual(sweden('701063-2391').getBirthday(), '1970-10-03')

        done()
      })
    })

    describe('isCoordinationNo', () => {
      it('Finds true coordination numbers', (done) => {
        assert.isTrue(sweden('701063-2391').isCoordinationNo())
        assert.isTrue(sweden('19701063-2391', false).isCoordinationNo())

        done()
      })

      it('Valid personal numbers are not valid coordination numbers', (done) => {
        assert.isFalse(sweden('470304-2657').isCoordinationNo())

        done()
      })

      it('Valid personal numbers are not valid coordination numbers', (done) => {
        assert.isFalse(sweden('160718-7566').isCoordinationNo())

        done()
      })
    })
  })
})
