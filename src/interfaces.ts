/* eslint no-undef: "off" */

// Simple Functions

type CalculateAgeFunc = (birthday: string, today: string) => number

type CalculateBitCheckSum = (str: string, numbers: number[]) => number

type CalculateLuhnChecksumFunc = (input: string) => number

type GetAgeFunc = (today?: string) => number

type GetBirthdayFunc = () => string

type GetCensoredFunc = () => string

type GetCountryFunc = () => string

type GetGenderFunc = () => string

type GetYearFunc = () => number

type IsCoordinationNoFunc = () => boolean

type IsDNumberFunc = () => boolean

type StringTransformationFunc = (input: string) => string

type ValidateBitChecksumFunc = (personalNumber: string, numbers: number[], mod?: number) => boolean

type ValidateDateFunc = (day: number, month: number, year: number) => boolean

type ValidateFunc = () => boolean

// Simple Interfaces

interface GetBirthdayFactoryInput {
  day: string
  month: string
}

interface GetGenderFactoryInput {
  gender: string
}

// The rest

interface CountryTools {
  getAge: GetAgeFunc
  getBirthday: GetBirthdayFunc
  getCensored: GetCensoredFunc
  getCountry: GetCountryFunc
  getGender: GetGenderFunc
  getYear: GetYearFunc
  isCoordinationNo?: IsCoordinationNoFunc
  isDNumber?: IsDNumberFunc
  validate: ValidateFunc
}

type CountryFunc = (personalNumber: string, strict?: boolean) => CountryTools

type DefaultToolsFunc = (extraTools?: object) => CountryTools

type GetAgeFactoryFunc = (getBirthday: GetBirthdayFunc) => GetAgeFunc

type GetBirthdayFactoryFunc = (parts: GetBirthdayFactoryInput, getYear: GetYearFunc) => GetBirthdayFunc

type GetGenderFactoryFunc = (parts: GetGenderFactoryInput) => GetGenderFunc

type ValidateFactoryFunc = (
  personalNumber: string,
  regex: RegExp,
  parts: GetBirthdayFactoryInput,
  getYear: GetYearFunc,
  localValidate: ValidateFunc
) => ValidateFunc

type ValidateLuhnNumber = (input: string) => boolean

export {
  CalculateAgeFunc,
  CalculateBitCheckSum,
  CalculateLuhnChecksumFunc,
  CountryFunc,
  CountryTools,
  DefaultToolsFunc,
  GetAgeFactoryFunc,
  GetAgeFunc,
  GetBirthdayFactoryFunc,
  GetBirthdayFactoryInput,
  GetBirthdayFunc,
  GetCensoredFunc,
  GetCountryFunc,
  GetGenderFactoryFunc,
  GetGenderFactoryInput,
  GetGenderFunc,
  GetYearFunc,
  IsCoordinationNoFunc,
  IsDNumberFunc,
  StringTransformationFunc,
  ValidateBitChecksumFunc,
  ValidateDateFunc,
  ValidateFactoryFunc,
  ValidateFunc,
  ValidateLuhnNumber
}
