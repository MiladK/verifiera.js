/* tslint:disable:arrow-call-signature */

import { assert } from 'chai'
import { describe, it } from 'mocha'

import {
  detectCountry,
  getCountryTools
} from './index'

const validDenmark = '120917-3806'
const validFinland = '311280-888Y'
const validNetherlands = '111222333'
const validNorway = '22071799674'
const validPoland = '31020908809'
const validSweden = '571124-1322'
const invalid = '44051401356'

describe('index', () => {
  describe('detectCountry', () => {
    it('detects valid personal numbers from Czech / Slovakia', (done) => {
      assert.strictEqual(detectCountry('736028/5163'), 'czech_slovakia')

      done()
    })

    it('detects valid personal numbers from Denmark (strict = true)', (done) => {
      assert.strictEqual(detectCountry('120917-3804'), 'denmark')

      done()
    })

    it('detects valid personal numbers from Denmark (strict = false)', (done) => {
      assert.strictEqual(detectCountry(validDenmark), 'denmark')

      done()
    })

    it('detects valid personal numbers from Finland', (done) => {
      assert.strictEqual(detectCountry(validFinland), 'finland')

      done()
    })

    it('detects valid personal numbers from Netherlands', (done) => {
      assert.strictEqual(detectCountry(validNetherlands), 'netherlands')

      done()
    })

    it('detects valid personal numbers from Norway', (done) => {
      assert.strictEqual(detectCountry(validNorway), 'norway')

      done()
    })

    it('detects valid personal numbers from Poland', (done) => {
      assert.strictEqual(detectCountry(validPoland), 'poland')

      done()
    })

    it('detects valid personal numbers from Sweden (strict = false)', (done) => {
      assert.strictEqual(detectCountry('19571124-1322'), 'sweden')

      done()
    })

    it('detects valid personal numbers from Sweden', (done) => {
      assert.strictEqual(detectCountry(validSweden), 'sweden')

      done()
    })

    it('returns empty string for invalid personal numbers', (done) => {
      assert.strictEqual(detectCountry(invalid), '')

      done()
    })
  })

  describe('getCountryTools', () => {
    it('detects valid personal numbers from Czech / Slovakia and returns countryTools', (done) => {
      const ssn = '736028/5163'

      assert.isTrue(getCountryTools(ssn).validate())
      assert.strictEqual(getCountryTools(ssn).getAge('2020-02-14'), 46)
      assert.strictEqual(getCountryTools(ssn).getBirthday(), '1973-10-28')
      assert.strictEqual(getCountryTools(ssn).getCensored(), '736028/****')
      assert.strictEqual(getCountryTools(ssn).getGender(), 'f')
      assert.strictEqual(getCountryTools(ssn).getYear(), 1973)

      done()
    })

    it('detects valid personal numbers from Denmark and returns countryTools (strict = true)', (done) => {
      const ssn = '120917-3804'

      assert.isTrue(getCountryTools(ssn).validate())
      assert.strictEqual(getCountryTools(ssn).getAge('2020-02-14'), 102)
      assert.strictEqual(getCountryTools(ssn).getBirthday(), '1917-09-12')
      assert.strictEqual(getCountryTools(ssn).getCensored(), '120917-****')
      assert.strictEqual(getCountryTools(ssn).getGender(), 'f')
      assert.strictEqual(getCountryTools(ssn).getYear(), 1917)

      done()
    })

    it('detects valid personal numbers from Denmark and returns countryTools (strict = false)', (done) => {
      const ssn = '120917-3806'

      assert.isTrue(getCountryTools(ssn).validate())
      assert.strictEqual(getCountryTools(ssn).getAge('2020-02-14'), 102)
      assert.strictEqual(getCountryTools(ssn).getBirthday(), '1917-09-12')
      assert.strictEqual(getCountryTools(ssn).getCensored(), '120917-****')
      assert.strictEqual(getCountryTools(ssn).getGender(), 'f')
      assert.strictEqual(getCountryTools(ssn).getYear(), 1917)

      done()
    })

    it('detects valid personal numbers from Finland and returns countryTools', (done) => {
      const ssn = validFinland

      assert.isTrue(getCountryTools(ssn).validate())
      assert.strictEqual(getCountryTools(ssn).getAge('2020-02-14'), 39)
      assert.strictEqual(getCountryTools(ssn).getBirthday(), '1980-12-31')
      assert.strictEqual(getCountryTools(ssn).getCensored(), '311280-****')
      assert.strictEqual(getCountryTools(ssn).getGender(), 'f')
      assert.strictEqual(getCountryTools(ssn).getYear(), 1980)

      done()
    })

    it('detects valid personal numbers from Netherlands and returns countryTools', (done) => {
      const ssn = validNetherlands

      assert.isTrue(getCountryTools(ssn).validate())
      assert.strictEqual(getCountryTools(ssn).getAge('2020-02-14'), 0)
      assert.strictEqual(getCountryTools(ssn).getBirthday(), '')
      assert.strictEqual(getCountryTools(ssn).getCensored(), '*********')
      assert.strictEqual(getCountryTools(ssn).getGender(), '')
      assert.strictEqual(getCountryTools(ssn).getYear(), 0)

      done()
    })

    it('detects valid personal numbers from Norway and returns countryTools', (done) => {
      const ssn = validNorway

      assert.isTrue(getCountryTools(ssn).validate())
      assert.strictEqual(getCountryTools(ssn).getAge('2020-02-14'), 2)
      assert.strictEqual(getCountryTools(ssn).getBirthday(), '2017-07-22')
      assert.strictEqual(getCountryTools(ssn).getCensored(), '220717*****')
      assert.strictEqual(getCountryTools(ssn).getGender(), 'f')
      assert.strictEqual(getCountryTools(ssn).getYear(), 2017)

      done()
    })

    it('detects valid personal numbers from Poland and returns countryTools', (done) => {
      const ssn = validPoland

      assert.isTrue(getCountryTools(ssn).validate())
      assert.strictEqual(getCountryTools(ssn).getAge('2020-02-14'), 89)
      assert.strictEqual(getCountryTools(ssn).getBirthday(), '1931-02-09')
      assert.strictEqual(getCountryTools(ssn).getCensored(), '310209*****')
      assert.strictEqual(getCountryTools(ssn).getGender(), 'f')
      assert.strictEqual(getCountryTools(ssn).getYear(), 1931)

      done()
    })

    it('detects valid personal numbers from Sweden and returns countryTools', (done) => {
      const ssn = validSweden

      assert.isTrue(getCountryTools(ssn).validate())
      assert.strictEqual(getCountryTools(ssn).getAge('2020-02-14'), 62)
      assert.strictEqual(getCountryTools(ssn).getBirthday(), '1957-11-24')
      assert.strictEqual(getCountryTools(ssn).getCensored(), '571124-****')
      assert.strictEqual(getCountryTools(ssn).getGender(), 'f')
      assert.strictEqual(getCountryTools(ssn).getYear(), 1957)

      done()
    })

    it('returns empty string for invalid personal numbers and returns default countryTools', (done) => {
      const ssn = invalid

      assert.isFalse(getCountryTools(ssn).validate())
      assert.strictEqual(getCountryTools(ssn).getAge('2020-02-14'), 0)
      assert.strictEqual(getCountryTools(ssn).getBirthday(), '')
      assert.strictEqual(getCountryTools(ssn).getCensored(), '')
      assert.strictEqual(getCountryTools(ssn).getGender(), '')
      assert.strictEqual(getCountryTools(ssn).getYear(), 0)

      done()
    })
  })
})
