/* tslint:disable:arrow-call-signature */

import { assert } from 'chai'
import { describe, it } from 'mocha'

import {
  isLuhnValid,
  luhnChecksum
} from './luhnAlgorithm'

describe('LuhnAlgorithm', () => {
  describe('isLuhnValid', () => {
    it('Checks if numbers are Luhn-Valid', (done) => {
      assert.isFalse(isLuhnValid('79927398710'))
      assert.isFalse(isLuhnValid('79927398711'))
      assert.isFalse(isLuhnValid('79927398712'))
      assert.isFalse(isLuhnValid('79927398714'))
      assert.isFalse(isLuhnValid('79927398715'))
      assert.isFalse(isLuhnValid('79927398716'))
      assert.isFalse(isLuhnValid('79927398717'))
      assert.isFalse(isLuhnValid('79927398718'))
      assert.isFalse(isLuhnValid('79927398719'))

      assert.isTrue(isLuhnValid('79927398713'))
      assert.isTrue(isLuhnValid('378282246310005'))
      assert.isTrue(isLuhnValid('371449635398431'))
      assert.isTrue(isLuhnValid('378734493671000'))
      assert.isTrue(isLuhnValid('5610591081018250'))
      assert.isTrue(isLuhnValid('30569309025904'))
      assert.isTrue(isLuhnValid('38520000023237'))
      assert.isTrue(isLuhnValid('6011111111111117'))
      assert.isTrue(isLuhnValid('6011000990139424'))
      assert.isTrue(isLuhnValid('3530111333300000'))
      assert.isTrue(isLuhnValid('3566002020360505'))
      assert.isTrue(isLuhnValid('5555555555554444'))
      assert.isTrue(isLuhnValid('5105105105105100'))
      assert.isTrue(isLuhnValid('4111111111111111'))
      assert.isTrue(isLuhnValid('4012888888881881'))
      assert.isTrue(isLuhnValid('4222222222222'))
      assert.isTrue(isLuhnValid('5019717010103742'))
      assert.isTrue(isLuhnValid('6331101999990016'))

      done()
    })
  })

  describe('luhnChecksum', () => {
    it('Reverse strings', (done) => {
      assert.strictEqual(luhnChecksum('79927398713'), 0)
      assert.strictEqual(luhnChecksum('378282246310005'), 0)
      assert.strictEqual(luhnChecksum('371449635398431'), 0)
      assert.strictEqual(luhnChecksum('378734493671000'), 0)
      assert.strictEqual(luhnChecksum('5610591081018250'), 0)
      assert.strictEqual(luhnChecksum('30569309025904'), 0)
      assert.strictEqual(luhnChecksum('38520000023237'), 0)
      assert.strictEqual(luhnChecksum('6011111111111117'), 0)
      assert.strictEqual(luhnChecksum('6011000990139424'), 0)
      assert.strictEqual(luhnChecksum('3530111333300000'), 0)
      assert.strictEqual(luhnChecksum('3566002020360505'), 0)
      assert.strictEqual(luhnChecksum('5555555555554444'), 0)
      assert.strictEqual(luhnChecksum('5105105105105100'), 0)
      assert.strictEqual(luhnChecksum('4111111111111111'), 0)
      assert.strictEqual(luhnChecksum('4012888888881881'), 0)
      assert.strictEqual(luhnChecksum('4222222222222'), 0)
      assert.strictEqual(luhnChecksum('5019717010103742'), 0)
      assert.strictEqual(luhnChecksum('6331101999990016'), 0)
      assert.strictEqual(luhnChecksum('49927398716'), 0)
      assert.strictEqual(luhnChecksum('470304265'), 3)
      assert.strictEqual(luhnChecksum('1234567'), 1)

      done()
    })
  })
})
