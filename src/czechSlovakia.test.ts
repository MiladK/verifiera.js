/* tslint:disable:arrow-call-signature */

import { assert } from 'chai'
import { describe, it } from 'mocha'

import { czechSlovakia } from './index'

describe('Czech / Slovakia', () => {
  describe('Validation', () => {
    it('Invalidates empty strings', (done) => {
      assert.isFalse(czechSlovakia('').validate())

      done()
    })

    it('Personal numbers before 1954 are automatically valid', (done) => {
      assert.isTrue(czechSlovakia('336028/000').validate())

      done()
    })

    it('Identifies invalid personal numbers', (done) => {
      assert.isFalse(czechSlovakia('736028/5161').validate())

      done()
    })

    it('Identifies valid personal numbers', (done) => {
      assert.isTrue(czechSlovakia('736028/5163').validate())
      assert.isTrue(czechSlovakia('236028/5169').validate())
      assert.isTrue(czechSlovakia('195928/5163').validate())
      assert.isTrue(czechSlovakia('190928/5169').validate())

      done()
    })
  })

  describe('getYear', () => {
    it('Calculates the year correctly', (done) => {
      assert.strictEqual(czechSlovakia('736028/5163').getYear(), 1973)
      assert.strictEqual(czechSlovakia('236028/5169').getYear(), 2023)
      assert.strictEqual(czechSlovakia('195928/5163').getYear(), 2019)
      assert.strictEqual(czechSlovakia('336028/000').getYear(), 1933)
      assert.strictEqual(czechSlovakia('736028/000').getYear(), 1873)

      done()
    })
  })

  describe('getBirthday', () => {
    it('Calculates the year correctly', (done) => {
      assert.strictEqual(czechSlovakia('736028/5163').getBirthday(), '1973-10-28')
      assert.strictEqual(czechSlovakia('236028/5169').getBirthday(), '2023-10-28')
      assert.strictEqual(czechSlovakia('195928/5163').getBirthday(), '2019-09-28')
      assert.strictEqual(czechSlovakia('331028/000').getBirthday(), '1933-10-28')
      assert.strictEqual(czechSlovakia('730928/000').getBirthday(), '1873-09-28')

      done()
    })
  })

  describe('getAge', () => {
    it('Calculates the age correctly', (done) => {
      const today = '2027-07-19'

      assert.strictEqual(czechSlovakia('736028/5163').getAge(today), 53)
      assert.strictEqual(czechSlovakia('236028/5169').getAge(today), 3)
      assert.strictEqual(czechSlovakia('195928/5163').getAge(today), 7)
      assert.strictEqual(czechSlovakia('336028/000').getAge(today), 93)
      assert.strictEqual(czechSlovakia('736028/000').getAge(today), 153)

      done()
    })
  })

  describe('getGender', () => {
    it('Identifies gender correctly', (done) => {
      assert.strictEqual(czechSlovakia('736028/5163').getGender(), 'f')
      assert.strictEqual(czechSlovakia('236028/5169').getGender(), 'f')
      assert.strictEqual(czechSlovakia('190928/5169').getGender(), 'm')
      assert.strictEqual(czechSlovakia('195928/5163').getGender(), 'f')
      assert.strictEqual(czechSlovakia('336028/000').getGender(), 'f')
      assert.strictEqual(czechSlovakia('736028/000').getGender(), 'f')

      done()
    })
  })

  describe('getCensored', () => {
    it('Identifies gender correctly', (done) => {
      assert.strictEqual(czechSlovakia('736028/5163').getCensored(), '736028/****')
      assert.strictEqual(czechSlovakia('236028/5169').getCensored(), '236028/****')
      assert.strictEqual(czechSlovakia('190928/5169').getCensored(), '190928/****')
      assert.strictEqual(czechSlovakia('195928/5163').getCensored(), '195928/****')
      assert.strictEqual(czechSlovakia('336028/000').getCensored(), '336028/****')
      assert.strictEqual(czechSlovakia('736028/000').getCensored(), '736028/****')

      done()
    })
  })
})
