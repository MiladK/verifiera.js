/* tslint:disable:arrow-call-signature */

import { assert } from 'chai'
import { describe, it } from 'mocha'

import { norway } from './index'

describe('Norway', () => {
  describe('isDNumber', () => {
    it('identifies D-numbers', (done) => {
      assert.isTrue(norway('41018036736').isDNumber())

      done()
    })

    it('identifies valid non-D-numbers', (done) => {
      assert.isFalse(norway('22071799321').isDNumber())

      done()
    })

    it('returns false for non-valid Norwegian numbers', (done) => {
      assert.isFalse(norway('03119975255').isDNumber())

      done()
    })
  })

  describe('getGender', () => {
    it('Gets the correct gender', (done) => {
      assert.strictEqual(norway('22071799674').getGender(), 'f')
      assert.strictEqual(norway('22071799402').getGender(), 'f')
      assert.strictEqual(norway('22071799240').getGender(), 'f')
      assert.strictEqual(norway('29029600013').getGender(), 'f')
      assert.strictEqual(norway('23047652480').getGender(), 'f')
      assert.strictEqual(norway('17099033487').getGender(), 'f')
      assert.strictEqual(norway('07076627678').getGender(), 'f')
      assert.strictEqual(norway('26090380809').getGender(), 'f')

      assert.strictEqual(norway('22071799755').getGender(), 'm')
      assert.strictEqual(norway('22071799593').getGender(), 'm')
      assert.strictEqual(norway('22071799321').getGender(), 'm')
      assert.strictEqual(norway('05084115322').getGender(), 'm')
      assert.strictEqual(norway('21034535105').getGender(), 'm')
      assert.strictEqual(norway('14090790987').getGender(), 'm')
      assert.strictEqual(norway('25127115195').getGender(), 'm')
      assert.strictEqual(norway('18112481504').getGender(), 'm')

      done()
    })

    it('Invalid personal numbers return empty gender', (done) => {
      assert.strictEqual(norway('17047000658').getGender(), '')

      done()
    })
  })

  describe('getCensored', () => {
    it('Censors personal numbers', (done) => {
      assert.strictEqual(norway('22071799674').getCensored(), '220717*****')
      assert.strictEqual(norway('22071799402').getCensored(), '220717*****')
      assert.strictEqual(norway('22071799240').getCensored(), '220717*****')
      assert.strictEqual(norway('29029600013').getCensored(), '290296*****')
      assert.strictEqual(norway('22071799755').getCensored(), '220717*****')
      assert.strictEqual(norway('22071799593').getCensored(), '220717*****')
      assert.strictEqual(norway('22071799321').getCensored(), '220717*****')

      done()
    })

    it('Invalid personal numbers return empty string', (done) => {
      assert.strictEqual(norway('17047000658').getCensored(), '')

      done()
    })
  })

  describe('getAge', () => {
    it('Calculates correct age', (done) => {
      const today = '2017-07-19'

      assert.strictEqual(norway('22071799674').getAge(today), 0)
      assert.strictEqual(norway('22071799402').getAge(today), 0)
      assert.strictEqual(norway('22071799240').getAge(today), 0)
      assert.strictEqual(norway('22071799755').getAge(today), 0)
      assert.strictEqual(norway('22071799593').getAge(today), 0)
      assert.strictEqual(norway('22071799321').getAge(today), 0)
      assert.strictEqual(norway('03111590925').getAge(today), 0)
      assert.strictEqual(norway('29101353689').getAge(today), 3)
      assert.strictEqual(norway('15090869180').getAge(today), 8)
      assert.strictEqual(norway('12050464596').getAge(today), 13)
      assert.strictEqual(norway('15070091884').getAge(today), 17)
      assert.strictEqual(norway('12119806192').getAge(today), 18)
      assert.strictEqual(norway('29029600013').getAge(today), 21)
      assert.strictEqual(norway('11115328435').getAge(today), 63)
      assert.strictEqual(norway('27124939173').getAge(today), 67)
      assert.strictEqual(norway('15104491526').getAge(today), 72)
      assert.strictEqual(norway('05084115322').getAge(today), 75)
      assert.strictEqual(norway('22103238602').getAge(today), 84)
      assert.strictEqual(norway('31031022188').getAge(today), 107)
      assert.strictEqual(norway('01010114388').getAge(today), 116)
      assert.strictEqual(norway('01010116550').getAge(today), 116)
      assert.strictEqual(norway('01010149939').getAge(today), 116)
      assert.strictEqual(norway('17037174246').getAge(today), 146)
      assert.strictEqual(norway('14036950049').getAge(today), 148)

      done()
    })

    it('Invalid personal numbers return 0 age', (done) => {
      const today = '2017-07-19'

      assert.strictEqual(norway('17047000658').getAge(today), 0)

      done()
    })
  })

  describe('Validation', () => {
    it('Invalidates empty strings', (done) => {
      assert.isFalse(norway('').validate())

      done()
    })

    it('Identifies invalid personal numbers', (done) => {
      assert.isFalse(norway('03111590925').validate())
      assert.isFalse(norway('03110175225').validate())
      assert.isFalse(norway('01129955131').validate())
      assert.isFalse(norway('0311997525E').validate())
      assert.isFalse(norway('2207179967').validate())
      assert.isFalse(norway('22071799325').validate())
      assert.isFalse(norway('03119975255').validate())
      assert.isFalse(norway('17047000658').validate())
      assert.isFalse(norway('18099805991').validate())
      assert.isFalse(norway('13124717928').validate())

      done()
    })

    it('Identifies valid personal numbers', (done) => {
      assert.isTrue(norway('22071799674').validate())
      assert.isTrue(norway('22071799402').validate())
      assert.isTrue(norway('22071799240').validate())
      assert.isTrue(norway('29029600013').validate())
      assert.isTrue(norway('22071799755').validate())
      assert.isTrue(norway('22071799593').validate())
      assert.isTrue(norway('22071799321').validate())

      done()
    })
  })

  describe('D-nummer', () => {
    const dNumber = '41018036736'

    it('Identifies valid D-nummer', (done) => {
      assert.isTrue(norway(dNumber).validate())

      done()
    })

    it('Identifies valid D-nummer', (done) => {
      assert.isTrue(norway(dNumber).isDNumber())

      done()
    })

    it('Identifies correct gender', (done) => {
      assert.strictEqual(norway(dNumber).getGender(), 'm')

      done()
    })

    it('Censors D-numbers', (done) => {
      assert.strictEqual(norway(dNumber).getCensored(), '410180*****')

      done()
    })

    it('Calculates correct age', (done) => {
      const today = '2017-07-19'

      assert.strictEqual(norway(dNumber).getAge(today), 37)

      done()
    })

    it('Calculates correct birthday', (done) => {
      assert.strictEqual(norway(dNumber).getBirthday(), '1980-01-01')

      done()
    })
  })
})
