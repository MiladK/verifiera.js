/* tslint:disable:arrow-call-signature */

import { assert } from 'chai'
import { describe, it } from 'mocha'

import { netherlands } from './index'

describe('Netherlands', () => {
  describe('getGender', () => {
    it('Returns empty gender', (done) => {
      assert.strictEqual(netherlands('111222333').getGender(), '')
      assert.strictEqual(netherlands('123456782').getGender(), '')

      done()
    })
  })

  describe('getCensored', () => {
    it('Does not change the personal number', (done) => {
      assert.strictEqual(netherlands('111222333').getCensored(), '*********')
      assert.strictEqual(netherlands('123456782').getCensored(), '*********')

      done()
    })
  })

  describe('getAge', () => {
    it('Returns zero', (done) => {
      const today = '2017-07-19'

      assert.strictEqual(netherlands('111222333').getAge(today), 0)
      assert.strictEqual(netherlands('123456782').getAge(today), 0)

      done()
    })
  })

  describe('Validation', () => {
    it('Invalidates empty strings', (done) => {
      assert.isFalse(netherlands('').validate())

      done()
    })

    it('Identifies invalid personal numbers', (done) => {
      assert.isFalse(netherlands('123456781').validate())

      done()
    })

    it('Identifies valid personal numbers', (done) => {
      assert.isTrue(netherlands('111222333').validate())
      assert.isTrue(netherlands('123456782').validate())

      done()
    })
  })
})
