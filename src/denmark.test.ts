/* tslint:disable:arrow-call-signature */

import { assert } from 'chai'
import { describe, it } from 'mocha'

import { denmark } from './index'

describe('Denmark', () => {
  describe('getYear', () => {
    it('focus on the year', (done) => {
      assert.strictEqual(denmark('010110-0731', true).getYear(), 1910)
      assert.strictEqual(denmark('010110-1738', true).getYear(), 1910)
      assert.strictEqual(denmark('010110-2734', true).getYear(), 1910)
      assert.strictEqual(denmark('010110-3730', true).getYear(), 1910)

      assert.strictEqual(denmark('010140-0735', true).getYear(), 1940)
      assert.strictEqual(denmark('010140-1731', true).getYear(), 1940)
      assert.strictEqual(denmark('010140-2738', true).getYear(), 1940)
      assert.strictEqual(denmark('010140-3734', true).getYear(), 1940)

      assert.strictEqual(denmark('010170-0739', true).getYear(), 1970)
      assert.strictEqual(denmark('010170-1735', true).getYear(), 1970)
      assert.strictEqual(denmark('010170-2731', true).getYear(), 1970)
      assert.strictEqual(denmark('010170-3738', true).getYear(), 1970)

      assert.strictEqual(denmark('010136-4739', true).getYear(), 2036)
      assert.strictEqual(denmark('010137-4734', true).getYear(), 1937)

      assert.strictEqual(denmark('010156-5734', true).getYear(), 2056)
      assert.strictEqual(denmark('010158-5735', true).getYear(), 1858)

      assert.strictEqual(denmark('010157-6736', true).getYear(), 2057)
      assert.strictEqual(denmark('010158-6731', true).getYear(), 1858)

      assert.strictEqual(denmark('010157-7732', true).getYear(), 2057)
      assert.strictEqual(denmark('010158-7738', true).getYear(), 1858)

      assert.strictEqual(denmark('010157-8739', true).getYear(), 2057)
      assert.strictEqual(denmark('010158-8734', true).getYear(), 1858)

      assert.strictEqual(denmark('010136-9730', true).getYear(), 2036)
      assert.strictEqual(denmark('010137-9736', true).getYear(), 1937)

      done()
    })
  })

  describe('getGender', () => {
    it('Gets the correct gender', (done) => {
      assert.strictEqual(denmark('130600-5738').getGender(), 'f')
      assert.strictEqual(denmark('120494-3806').getGender(), 'f')
      assert.strictEqual(denmark('310586-4948').getGender(), 'f')
      assert.strictEqual(denmark('120917-3804').getGender(), 'f')
      assert.strictEqual(denmark('150517-3712').getGender(), 'f')

      assert.strictEqual(denmark('070777-1119').getGender(), 'm')
      assert.strictEqual(denmark('220890-4895').getGender(), 'm')
      assert.strictEqual(denmark('171263-1615').getGender(), 'm')

      done()
    })

    it('Invalid personal numbers return empty gender', (done) => {
      assert.strictEqual(denmark('360783-1234').getGender(), '')

      done()
    })
  })

  describe('getCensored', () => {
    it('Censors personal numbers', (done) => {
      assert.strictEqual(denmark('130600-5738').getCensored(), '130600-****')
      assert.strictEqual(denmark('070777-1119').getCensored(), '070777-****')

      done()
    })

    it('Invalid personal numbers return empty string', (done) => {
      assert.strictEqual(denmark('360783-1234').getCensored(), '')

      done()
    })
  })

  describe('getAge', () => {
    const today = '2017-07-19'

    it('Calculates correct age', (done) => {
      assert.strictEqual(denmark('130600-5738').getAge(today), 17)
      assert.strictEqual(denmark('120494-3806').getAge(today), 23)
      assert.strictEqual(denmark('220890-4895').getAge(today), 26)
      assert.strictEqual(denmark('310586-4948').getAge(today), 31)
      assert.strictEqual(denmark('070777-1119').getAge(today), 40)
      assert.strictEqual(denmark('171263-1615').getAge(today), 53)
      assert.strictEqual(denmark('120917-3804').getAge(today), 99)
      assert.strictEqual(denmark('150517-3712').getAge(today), 100)
      assert.strictEqual(denmark('211062-5629').getAge(today), 154)

      done()
    })

    it('Invalid personal numbers return 0 age', (done) => {
      assert.strictEqual(denmark('360783-1234').getAge(today), 0)

      done()
    })
  })

  describe('Validation', () => {
    it('Invalidates empty strings', (done) => {
      assert.isFalse(denmark('').validate())

      done()
    })

    it('Identifies invalid personal numbers', (done) => {
      assert.isFalse(denmark('300294-3806').validate())

      done()
    })

    it('Identifies valid personal numbers', (done) => {
      assert.isTrue(denmark('130600-5738').validate())
      assert.isTrue(denmark('070777-1119').validate())
      assert.isTrue(denmark('260783-1234').validate())
      assert.isTrue(denmark('120494-3806').validate())
      assert.isTrue(denmark('220890-4895').validate())
      assert.isTrue(denmark('310586-4948').validate())
      assert.isTrue(denmark('171263-1615').validate())
      assert.isTrue(denmark('150517-3712').validate())
      assert.isTrue(denmark('120917-3804').validate())

      done()
    })

    describe('strict switch', () => {
      it('strict = true', (done) => {
        assert.isFalse(denmark('260783-1234', true).validate())

        done()
      })

      it('strict = false (default)', (done) => {
        assert.isTrue(denmark('260783-1234').validate())

        done()
      })
    })
  })
})
