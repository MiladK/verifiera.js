/* tslint:disable:arrow-call-signature */

import { assert } from 'chai'
import { describe, it } from 'mocha'

import {
  clean,
  reverseString,
  validateBitCheckSum,
  validateDate
} from './utilities'

describe('Utilities', () => {
  it('clean', (done) => {
    assert.strictEqual(clean('311280-888Y'), '311280888')
    assert.strictEqual(clean('010808A704V'), '010808704')

    done()
  })

  it('reverseString', (done) => {
    assert.strictEqual(reverseString(''), '')
    assert.strictEqual(reverseString('verifiera'), 'areifirev')

    done()
  })

  it('validateDate()', (done) => {
    assert.isTrue(validateDate(28, 2, 2016))
    assert.isFalse(validateDate(29, 2, 2017))
    assert.isFalse(validateDate(29, 2, 2019))
    assert.isTrue(validateDate(29, 2, 2020))
    assert.isTrue(validateDate(28, 2, 2019))
    assert.isTrue(validateDate(1, 1, 2019))

    done()
  })

  it('validateBitCheckSum()', (done) => {
    assert.isTrue(validateBitCheckSum('12', [1, 2], 5))
    assert.isTrue(validateBitCheckSum('123', [1, 2], 5))
    assert.isTrue(validateBitCheckSum('123456789', [1, 2, 5, 6, 7, 6, 3, 7, 4], 3))
    assert.isTrue(validateBitCheckSum('29029600013', [3, 7, 6, 1, 8, 9, 4, 5, 2, 1], 11))

    done()
  })
})
