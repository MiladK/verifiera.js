/*!
 * verifiera
 * @license MIT
 * Copyright (c) 2020 Milad Kawas CALE. All rights reserved.
*/

import {
  CalculateAgeFunc,
  CalculateBitCheckSum,
  StringTransformationFunc,
  ValidateBitChecksumFunc,
  ValidateDateFunc
} from './interfaces'

const calculateAge: CalculateAgeFunc = (birthday: string, today: string): number => {
  const isLeapYear = (year: number): boolean => {
    const date = new Date(year, 1, 28)
    date.setDate(date.getDate() + 1)

    return date.getMonth() === 1
  }

  const d: Date = new Date(birthday)
  const now: Date = today ? new Date(today) : new Date()

  let years: number = now.getFullYear() - d.getFullYear()

  d.setFullYear(d.getFullYear() + years)

  if (d > now) {
    years -= 1
    d.setFullYear(d.getFullYear() - 1)
  }

  const days: number = (now.getTime() - d.getTime()) / (3600 * 24 * 1000)

  const age = years + days / (isLeapYear(now.getFullYear()) ? 366 : 365)

  return age > 0 ? Math.floor(age) : 0
}

const validateDate: ValidateDateFunc = (day: number, month: number, year: number): boolean => {
  const date: Date = new Date()
  date.setFullYear(year, month - 1, day)

  return date.getFullYear() === year &&
    date.getMonth() + 1 === month &&
    date.getDate() === day
}

const clean: StringTransformationFunc = (input: string): string => input.replace(/[^\d]/g, '')
  .trim()

const calculateBitCheckSum: CalculateBitCheckSum = (str: string, numbers: number[]): number => {
  const cleaned: string = clean(str)
  let sum = 0

  numbers.forEach((num: number, index: number) => {
    sum += num * parseInt(cleaned[index], 10)
  })

  return sum
}

const validateBitCheckSum: ValidateBitChecksumFunc = (
  personalNumber: string,
  numbers: number[],
  mod: number = 11
): boolean => calculateBitCheckSum(personalNumber, numbers) % mod === 0

const reverseString: StringTransformationFunc = (str: string): string => str.split('').reverse().join('')

export {
  calculateAge,
  calculateBitCheckSum,
  clean,
  reverseString,
  validateBitCheckSum,
  validateDate
}
