/* tslint:disable:arrow-call-signature */

import { assert } from 'chai'
import { describe, it } from 'mocha'

import { finland } from './index'

describe('Finland', () => {
  describe('getGender', () => {
    it('Gets the correct gender', (done) => {
      assert.strictEqual(finland('311280-888Y').getGender(), 'f')
      assert.strictEqual(finland('281194-0582').getGender(), 'f')
      assert.strictEqual(finland('200278-704B').getGender(), 'f')
      assert.strictEqual(finland('200278+704B').getGender(), 'f')
      assert.strictEqual(finland('010808A704V').getGender(), 'f')

      assert.strictEqual(finland('270846-627Y').getGender(), 'm')
      assert.strictEqual(finland('130781-4116').getGender(), 'm')
      assert.strictEqual(finland('280731-743N').getGender(), 'm')
      assert.strictEqual(finland('240696-797T').getGender(), 'm')
      assert.strictEqual(finland('280162-343X').getGender(), 'm')

      done()
    })

    it('Invalid personal numbers return empty gender', (done) => {
      assert.strictEqual(finland('311280-888U').getGender(), '')

      done()
    })
  })

  describe('getCensored', () => {
    it('Censors personal numbers', (done) => {
      assert.strictEqual(finland('311280-888Y').getCensored(), '311280-****')
      assert.strictEqual(finland('270846-627Y').getCensored(), '270846-****')
      assert.strictEqual(finland('281194-0582').getCensored(), '281194-****')
      assert.strictEqual(finland('130781-4116').getCensored(), '130781-****')
      assert.strictEqual(finland('280731-743N').getCensored(), '280731-****')
      assert.strictEqual(finland('240696-797T').getCensored(), '240696-****')
      assert.strictEqual(finland('280162-343X').getCensored(), '280162-****')
      assert.strictEqual(finland('200278-704B').getCensored(), '200278-****')
      assert.strictEqual(finland('200278+704B').getCensored(), '200278+****')
      assert.strictEqual(finland('010808A704V').getCensored(), '010808A****')

      done()
    })

    it('Invalid personal numbers return empty string', (done) => {
      assert.strictEqual(finland('311280-888U').getCensored(), '')

      done()
    })
  })

  describe('getAge', () => {
    it('Calculates correct age', (done) => {
      const today = '2017-07-19'

      assert.strictEqual(finland('010808A704V').getAge(today), 8)
      assert.strictEqual(finland('281194-0582').getAge(today), 22)
      assert.strictEqual(finland('130781-4116').getAge(today), 36)
      assert.strictEqual(finland('280731-743N').getAge(today), 85)
      assert.strictEqual(finland('240696+797T').getAge(today), 121)
      assert.strictEqual(finland('200278+704B').getAge(today), 139)

      done()
    })

    it('Invalid personal numbers return 0 age', (done) => {
      const today = '2017-07-19'

      assert.strictEqual(finland('311280-888U').getAge(today), 0)

      done()
    })
  })

  describe('Validation', () => {
    it('Invalidates empty strings', (done) => {
      assert.isFalse(finland('').validate())

      done()
    })

    it('Identifies invalid personal numbers', (done) => {
      assert.isFalse(finland('311280-888U').validate())

      done()
    })

    it('Identifies valid personal numbers', (done) => {
      assert.isTrue(finland('311280-888Y').validate())
      assert.isTrue(finland('270846-627Y').validate())
      assert.isTrue(finland('281194-0582').validate())
      assert.isTrue(finland('130781-4116').validate())
      assert.isTrue(finland('280731-743N').validate())
      assert.isTrue(finland('240696-797T').validate())
      assert.isTrue(finland('280162-343X').validate())
      assert.isTrue(finland('200278-704B').validate())
      assert.isTrue(finland('200278+704B').validate())
      assert.isTrue(finland('010808A704V').validate())

      done()
    })
  })
})
