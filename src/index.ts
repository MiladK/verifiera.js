import {
  defaultTools
} from './commonTools'
import czechSlovakia from './czechSlovakia'
import denmark from './denmark'
import finland from './finland'
import {
  CountryTools
} from './interfaces'
import netherlands from './netherlands'
import norway from './norway'
import poland from './poland'
import sweden from './sweden'

const countries = [
  czechSlovakia,
  denmark,
  finland,
  netherlands,
  norway,
  poland,
  sweden
]

const detectCountry = (personalNumber: string): string => {
  for (const country of countries) {
    if (country.length === 1) {
      const detector: CountryTools = country(personalNumber)

      if (detector.validate()) {
        return detector.getCountry()
      }
    } else if (country.length === 2) {
      const detector1: CountryTools = country(personalNumber, true)
      const detector2: CountryTools = country(personalNumber, false)

      if (detector1.validate()) {
        return detector1.getCountry()
      }

      if (detector2.validate()) {
        return detector2.getCountry()
      }
    }
  }

  return ''
}

const getCountryTools = (personalNumber: string): CountryTools => {
  for (const country of countries) {
    if (country.length === 1) {
      const detector: CountryTools = country(personalNumber)

      if (detector.validate()) {
        return detector
      }
    } else if (country.length === 2) {
      const detector1: CountryTools = country(personalNumber, true)
      const detector2: CountryTools = country(personalNumber, false)

      if (detector1.validate()) {
        return detector1
      }

      if (detector2.validate()) {
        return detector2
      }
    }
  }

  return defaultTools()
}

export {
  // Extra tools
  detectCountry,
  getCountryTools,
  // Countries
  czechSlovakia,
  denmark,
  finland,
  netherlands,
  norway,
  poland,
  sweden
}
