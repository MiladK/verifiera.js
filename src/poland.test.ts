/* tslint:disable:arrow-call-signature */

import { assert } from 'chai'
import { describe, it } from 'mocha'

import { poland } from './index'

describe('poland', () => {
  describe('getGender', () => {
    it('Gets the correct gender', (done) => {
      assert.strictEqual(poland('31020908809').getGender(), 'f')
      assert.strictEqual(poland('67262790801').getGender(), 'f')
      assert.strictEqual(poland('67090550002').getGender(), 'f')
      assert.strictEqual(poland('34230487249').getGender(), 'f')
      assert.strictEqual(poland('92012919764').getGender(), 'f')

      assert.strictEqual(poland('83622812330').getGender(), 'm')
      assert.strictEqual(poland('83422812334').getGender(), 'm')
      assert.strictEqual(poland('86091447412').getGender(), 'm')
      assert.strictEqual(poland('54111019473').getGender(), 'm')
      assert.strictEqual(poland('36030687715').getGender(), 'm')
      assert.strictEqual(poland('59061216433').getGender(), 'm')
      assert.strictEqual(poland('04020506179').getGender(), 'm')
      assert.strictEqual(poland('35020691714').getGender(), 'm')
      assert.strictEqual(poland('44051401458').getGender(), 'm')
      assert.strictEqual(poland('44051401458').getGender(), 'm')
      assert.strictEqual(poland('45260298217').getGender(), 'm')
      assert.strictEqual(poland('26321891950').getGender(), 'm')
      assert.strictEqual(poland('44051401359').getGender(), 'm')

      done()
    })

    it('Invalid personal numbers return empty gender', (done) => {
      assert.strictEqual(poland('35020691717').getGender(), '')

      done()
    })
  })

  describe('getCensored', () => {
    it('Censors personal numbers', (done) => {
      assert.strictEqual(poland('44051401359').getCensored(), '440514*****')
      assert.strictEqual(poland('31020908809').getCensored(), '310209*****')
      assert.strictEqual(poland('67262790801').getCensored(), '672627*****')
      assert.strictEqual(poland('67090550002').getCensored(), '670905*****')
      assert.strictEqual(poland('04020506179').getCensored(), '040205*****')
      assert.strictEqual(poland('34230487249').getCensored(), '342304*****')
      assert.strictEqual(poland('26321891950').getCensored(), '263218*****')
      assert.strictEqual(poland('45260298217').getCensored(), '452602*****')
      assert.strictEqual(poland('44051401458').getCensored(), '440514*****')
      assert.strictEqual(poland('44051401458').getCensored(), '440514*****')
      assert.strictEqual(poland('35020691714').getCensored(), '350206*****')
      assert.strictEqual(poland('92012919764').getCensored(), '920129*****')
      assert.strictEqual(poland('59061216433').getCensored(), '590612*****')
      assert.strictEqual(poland('36030687715').getCensored(), '360306*****')
      assert.strictEqual(poland('54111019473').getCensored(), '541110*****')
      assert.strictEqual(poland('86091447412').getCensored(), '860914*****')
      assert.strictEqual(poland('83422812334').getCensored(), '834228*****')
      assert.strictEqual(poland('83622812330').getCensored(), '836228*****')

      done()
    })

    it('Invalid personal numbers return empty string', (done) => {
      assert.strictEqual(poland('83622812334').getCensored(), '')

      done()
    })
  })

  describe('getAge', () => {
    it('Calculates correct age', (done) => {
      const today = '2017-07-19'

      assert.strictEqual(poland('44051401359').getAge(today), 73)
      assert.strictEqual(poland('31020908809').getAge(today), 86)
      assert.strictEqual(poland('67090550002').getAge(today), 49)
      assert.strictEqual(poland('04020506179').getAge(today), 113)
      assert.strictEqual(poland('44051401458').getAge(today), 73)
      assert.strictEqual(poland('35020691714').getAge(today), 82)
      assert.strictEqual(poland('92012919764').getAge(today), 25)
      assert.strictEqual(poland('59061216433').getAge(today), 58)
      assert.strictEqual(poland('36030687715').getAge(today), 81)
      assert.strictEqual(poland('54111019473').getAge(today), 62)
      assert.strictEqual(poland('86091447412').getAge(today), 30)

      done()
    })

    it('Invalid personal numbers return 0 age', (done) => {
      const today = '2017-07-19'

      assert.strictEqual(poland('59061216437').getAge(today), 0)

      done()
    })
  })

  describe('Validation', () => {
    it('Invalidates empty strings', (done) => {
      assert.isFalse(poland('').validate())

      done()
    })

    it('Identifies invalid personal numbers', (done) => {
      assert.isFalse(poland('44051401351').validate())
      assert.isFalse(poland('44051401352').validate())
      assert.isFalse(poland('44051401353').validate())
      assert.isFalse(poland('44051401354').validate())
      assert.isFalse(poland('44051401355').validate())
      assert.isFalse(poland('44051401356').validate())
      assert.isFalse(poland('44051401357').validate())
      assert.isFalse(poland('44051401358').validate())
      assert.isFalse(poland('44051401350').validate())
      assert.isFalse(poland('59101503402').validate())
      assert.isFalse(poland('91051223602').validate())
      assert.isFalse(poland('67010523892').validate())
      assert.isFalse(poland('98101267595').validate())
      assert.isFalse(poland('57120912947').validate())
      assert.isFalse(poland('82123301259').validate())
      assert.isFalse(poland('56120317609').validate())
      assert.isFalse(poland('87111909834').validate())
      assert.isFalse(poland('79092398739').validate())
      assert.isFalse(poland('66120822178').validate())
      assert.isFalse(poland('61121001236').validate())
      assert.isFalse(poland('44052401458').validate())
      assert.isFalse(poland('12345678909').validate())

      done()
    })

    it('Identifies valid personal numbers', (done) => {
      assert.isTrue(poland('44051401359').validate())
      assert.isTrue(poland('31020908809').validate())
      assert.isTrue(poland('67262790801').validate())
      assert.isTrue(poland('67090550002').validate())
      assert.isTrue(poland('04020506179').validate())
      assert.isTrue(poland('34230487249').validate())
      assert.isTrue(poland('26321891950').validate())
      assert.isTrue(poland('45260298217').validate())
      assert.isTrue(poland('44051401458').validate())
      assert.isTrue(poland('44051401458').validate())
      assert.isTrue(poland('35020691714').validate())
      assert.isTrue(poland('92012919764').validate())
      assert.isTrue(poland('59061216433').validate())
      assert.isTrue(poland('36030687715').validate())
      assert.isTrue(poland('54111019473').validate())
      assert.isTrue(poland('97091106384').validate())
      assert.isTrue(poland('86091447412').validate())
      assert.isTrue(poland('83422812334').validate())
      assert.isTrue(poland('83622812330').validate())

      done()
    })
  })
})
