import typescript from 'typescript'
import rollupTypeScript from 'rollup-plugin-typescript2'
import { uglify } from 'rollup-plugin-uglify'
import pkg from './package.json'

const compilerOptions = {
  lib: [
    'es2016'
  ],
  module: 'ES2015',
  target: 'es5',
  declaration: false
}

export default [
  {
    input: 'src/index.ts',
    plugins: [
      rollupTypeScript({
        typescript,
        tsconfigOverride: {
          compilerOptions: {
            ...compilerOptions,
            declaration: true
          }
        }
      })
    ],
    output: [
      {
        format: 'es',
        file: pkg.module
      }
    ]
  },
  {
    input: 'src/index.ts',
    plugins: [
      rollupTypeScript({
        typescript,
        tsconfigOverride: {
          compilerOptions: {
            ...compilerOptions
          }
        }
      })
    ],
    output: [
      {
        format: 'cjs',
        file: pkg.main
      }
    ]
  },
  {
    input: 'src/index.ts',
    plugins: [
      rollupTypeScript({
        typescript,
        tsconfigOverride: {
          compilerOptions: {
            ...compilerOptions
          }
        }
      }),
      uglify()
    ],
    output: [
      {
        format: 'umd',
        file: pkg.browser,
        sourcemap: true,
        name: 'verifiera'
      }
    ]
  }
]
