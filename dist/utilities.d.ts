/*!
 * verifiera
 * @license MIT
 * Copyright (c) 2020 Milad Kawas CALE. All rights reserved.
*/
import { CalculateAgeFunc, CalculateBitCheckSum, StringTransformationFunc, ValidateBitChecksumFunc, ValidateDateFunc } from './interfaces';
declare const calculateAge: CalculateAgeFunc;
declare const validateDate: ValidateDateFunc;
declare const clean: StringTransformationFunc;
declare const calculateBitCheckSum: CalculateBitCheckSum;
declare const validateBitCheckSum: ValidateBitChecksumFunc;
declare const reverseString: StringTransformationFunc;
export { calculateAge, calculateBitCheckSum, clean, reverseString, validateBitCheckSum, validateDate };
