declare type CalculateAgeFunc = (birthday: string, today: string) => number;
declare type CalculateBitCheckSum = (str: string, numbers: number[]) => number;
declare type CalculateLuhnChecksumFunc = (input: string) => number;
declare type GetAgeFunc = (today?: string) => number;
declare type GetBirthdayFunc = () => string;
declare type GetCensoredFunc = () => string;
declare type GetCountryFunc = () => string;
declare type GetGenderFunc = () => string;
declare type GetYearFunc = () => number;
declare type IsCoordinationNoFunc = () => boolean;
declare type IsDNumberFunc = () => boolean;
declare type StringTransformationFunc = (input: string) => string;
declare type ValidateBitChecksumFunc = (personalNumber: string, numbers: number[], mod?: number) => boolean;
declare type ValidateDateFunc = (day: number, month: number, year: number) => boolean;
declare type ValidateFunc = () => boolean;
interface GetBirthdayFactoryInput {
    day: string;
    month: string;
}
interface GetGenderFactoryInput {
    gender: string;
}
interface CountryTools {
    getAge: GetAgeFunc;
    getBirthday: GetBirthdayFunc;
    getCensored: GetCensoredFunc;
    getCountry: GetCountryFunc;
    getGender: GetGenderFunc;
    getYear: GetYearFunc;
    isCoordinationNo?: IsCoordinationNoFunc;
    isDNumber?: IsDNumberFunc;
    validate: ValidateFunc;
}
declare type CountryFunc = (personalNumber: string, strict?: boolean) => CountryTools;
declare type DefaultToolsFunc = (extraTools?: object) => CountryTools;
declare type GetAgeFactoryFunc = (getBirthday: GetBirthdayFunc) => GetAgeFunc;
declare type GetBirthdayFactoryFunc = (parts: GetBirthdayFactoryInput, getYear: GetYearFunc) => GetBirthdayFunc;
declare type GetGenderFactoryFunc = (parts: GetGenderFactoryInput) => GetGenderFunc;
declare type ValidateFactoryFunc = (personalNumber: string, regex: RegExp, parts: GetBirthdayFactoryInput, getYear: GetYearFunc, localValidate: ValidateFunc) => ValidateFunc;
declare type ValidateLuhnNumber = (input: string) => boolean;
export { CalculateAgeFunc, CalculateBitCheckSum, CalculateLuhnChecksumFunc, CountryFunc, CountryTools, DefaultToolsFunc, GetAgeFactoryFunc, GetAgeFunc, GetBirthdayFactoryFunc, GetBirthdayFactoryInput, GetBirthdayFunc, GetCensoredFunc, GetCountryFunc, GetGenderFactoryFunc, GetGenderFactoryInput, GetGenderFunc, GetYearFunc, IsCoordinationNoFunc, IsDNumberFunc, StringTransformationFunc, ValidateBitChecksumFunc, ValidateDateFunc, ValidateFactoryFunc, ValidateFunc, ValidateLuhnNumber };
