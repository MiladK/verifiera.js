import { DefaultToolsFunc, GetAgeFactoryFunc, GetBirthdayFactoryFunc, GetGenderFactoryFunc, IsCoordinationNoFunc, IsDNumberFunc, ValidateFactoryFunc } from './interfaces';
declare const defaultIsCoordinationNo: IsCoordinationNoFunc;
declare const defaultIsDNumber: IsDNumberFunc;
declare const getGenderFactory: GetGenderFactoryFunc;
declare const getBirthdayFactory: GetBirthdayFactoryFunc;
declare const getAgeFactory: GetAgeFactoryFunc;
declare const validateFactory: ValidateFactoryFunc;
declare const defaultTools: DefaultToolsFunc;
export { defaultIsCoordinationNo, defaultIsDNumber, defaultTools, getAgeFactory, getBirthdayFactory, getGenderFactory, validateFactory };
