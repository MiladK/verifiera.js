import { CalculateLuhnChecksumFunc, ValidateLuhnNumber } from './interfaces';
declare const luhnChecksum: CalculateLuhnChecksumFunc;
declare const isLuhnValid: ValidateLuhnNumber;
export { isLuhnValid, luhnChecksum };
